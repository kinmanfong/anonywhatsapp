var staticCacheName = 'site-static-v1.5';
var assets = [
    '/',
    '/index.html',
    '/master.css',
    '/whatsapp.js',
    '/WA.jpg',
    '/WA144.png',
    'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js',
    'https://code.jquery.com/jquery-3.5.1.slim.min.js',
    'https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js',
    'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
    'https://fonts.gstatic.com/s/montserrat/v24/JTUHjIg1_i6t8kCHKm4532VJOt5-QNFgpCtr6Hw5aXp-obK4.woff2',
];

self.addEventListener('install', evt => {
    // console.log('service worker has been installed');
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
        //console.log('caching shell assets');
        cache.addAll(assets);
    })
    );
});

self.addEventListener('activate', evt => {
    //console.log('service worker has been activated');
    evt.waitUntil(
        caches.keys().then(keys =>{
            //console.log(keys); 
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key))
                )
        })
    );
});

self.addEventListener('fetch', evt => {
    //console.log('fetch event', evt);
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request);
        })
    );
});

